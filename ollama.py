import httpx
import re

OLLAMA_URL = "http://localhost:11434/api/generate"

async def _call_api(prompt, model):
    async with httpx.AsyncClient(timeout=60.0) as client:
        data = {
            "prompt": prompt,
            "model": model,
            "stream": False,
            "options": {
                "num_predict": 256
            }
        }

        response = await client.post(url=OLLAMA_URL, headers={
            "Content-Type": "application/json",
        }, json=data)

        text = response.json()["response"]
        return text

async def call_codellama_api(prefix):
    
    prompt = f"""Complete the following code. Respond with the resultant code only: \n```{prefix}```"""

    response = await _call_api(prompt, "codellama")
    text = response.replace("<EOT>", "")

    regex = r"```(.+)```"
    m = re.search(regex, text, re.MULTILINE | re.DOTALL)
    if m:
        return m.group(1)
    
    return text

async def call_stable_code_api(prefix):
    
    prompt = f"""<fim_prefix>{prefix}<fim_suffix><fim_middle>"""

    response = await _call_api(prompt, "stable-code")
    text = response.replace("<EOT>", "")

    regex = r"```(.+)```"
    m = re.search(regex, text, re.MULTILINE | re.DOTALL)
    if m:
        return m.group(1)
    
    return prefix + "\n" + text

async def call_dolphin_mistral_api(prefix):
    
    prompt = f"""<|im_start|>system
You are an expert programmer. Complete the code provided by the user. Respond with code only - include the original code provided.<|im_end|>
<|im_start|>user
Complete the following code. Respond with the resultant code only:\n{prefix}<|im_end|>
<|im_start|>assistant
    """

    response = await _call_api(prompt, "dolphin-mistral")
    text = response.replace("<EOT>", "")

    regex = r"```python(.+)```"
    m = re.search(regex, text, re.MULTILINE | re.DOTALL)
    if m:
        text = m.group(1)
    
    if text.startswith(' '):
            text = text[1:]
    return text

async def call_deepseek_coder_api(prefix):
    
    prompt = f"""You are an expert programmer. Complete the code provided by the user. Respond with code only - include the original code provided.<|im_end|>
### Instruction:
Complete the following code. Respond with the resultant code only:\n{prefix}<|im_end|>
### Response:
    """

    response = await _call_api(prompt, "deepseek-coder:6.7b-instruct")
    text = response.replace("<EOT>", "")

    regex = r"```python(.+)```"
    m = re.search(regex, text, re.MULTILINE | re.DOTALL)
    if m:
        text = m.group(1)
    
    return text