import httpx
import os

GITLAB_URL = "https://gitlab.com/api/v4/code_suggestions/completions"

async def call_gitlab_api(prefix):
    async with httpx.AsyncClient(timeout=20.0) as client:
        token = os.getenv("GITLAB_TOKEN")
        data = {
            "intent": "generation",
            "current_file": {
                "file_name": "main.py",
                "content_above_cursor": prefix,
                "content_below_cursor": ""
            },
            "prompt_version": 1,
            "project_id": 0,
            "project_path": "",
            "stream": False
        }
        response = await client.post(url=GITLAB_URL, headers={
            "Content-Type": "application/json",
            "Authorization": f"Bearer {token}"
        }, json=data)

        text = response.json()["choices"][0]['text']
        lines = text.split("\n")
        if len(lines) <= 1 or not lines[1].startswith("    "):
            return prefix + "\n" + "    " + "\n    ".join(lines)
        else:
            return prefix + "\n" + text