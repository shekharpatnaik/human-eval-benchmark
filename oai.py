from openai import AsyncOpenAI

client = None

async def call_openai_api(prefix):
    if client == None:
        client = AsyncOpenAI()
    prompt = f"""Complete the following code. Respond with the resultant code only: \n```{prefix}```"""
    response = await client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{
            "role": "user",
            "content": prompt
        }],
        temperature=0.0
    )
    return response.choices[0].message.content