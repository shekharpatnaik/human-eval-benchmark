import re
from os.path import join

def _get_fn_name(prompt):
    regex = r"def\s(.+)\("
    return re.search(regex, prompt).group(1)

def check(prompt, api_result, test, index, debug, logs): 
    fn_name = _get_fn_name(prompt)
    program = api_result + "\n" + test + "\n" + f"check({fn_name})"
    if debug:
        with open(join(logs, f'{index}.py'), 'w') as f:
            f.write(program)
    try:
        exec(program)
        return "Passed"
    except:
        return "Failed"