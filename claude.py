from anthropic import AsyncAnthropic, HUMAN_PROMPT, AI_PROMPT
import re

client = None

async def call_claude_api(prefix):
    global client
    if client == None:
        client = AsyncAnthropic()

    prompt = f"""{HUMAN_PROMPT} Complete the following code. Respond with the resultant code only: \n```{prefix}```{AI_PROMPT}"""
    
    response = await client.completions.create(
        model="claude-2.1",
        prompt=prompt,
        max_tokens_to_sample=300
    )

    text = response.completion
    regex = r"```python(.+)```"
    m = re.search(regex, text, re.MULTILINE | re.DOTALL)
    if m:
        text = m.group(1)

    return text

async def call_messaging_api(prefix):
    global client
    if client == None:
        client = AsyncAnthropic()

    response = await client.messages.create(
        model="claude-2.1",
        max_tokens=1024,
        messages=[
            {"role": "user", "content": f"Complete the following code. Respond with the resultant code only: \n```{prefix}```"}
        ]
    )

    text = response.content[0].text
    
    regex = r"```python(.+)```"
    m = re.search(regex, text, re.MULTILINE | re.DOTALL)
    if m:
        text = m.group(1)

    return text