# Run Human Eval Benchmarks

This code runs the human eval benchmark against various LLMs

# Running the code

```sh
python -m venv venv
source ./venv/bin/activate
python main.py
python main.py --model=dolphin-mistral --limit=10
```

The `--limit` flag decides how many of the examples from the benchmark do you want to run, up to a max of 164. The `--debug` flag with write the final code to files in a `./logs` directory.