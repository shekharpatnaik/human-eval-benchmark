import asyncio
import fire
import sys
from datasets import load_dataset
from tabulate import tabulate
from tqdm.asyncio import tqdm_asyncio

from gitlab import call_gitlab_api
from oai import call_openai_api
from ollama import call_codellama_api, call_stable_code_api, call_dolphin_mistral_api, call_deepseek_coder_api
from claude import call_claude_api, call_messaging_api
from check import check

async def run_test(row, run_fn, i, debug, logs):
    api_result = await run_fn(row['prompt'])
    check_result = check(row['prompt'], api_result, row['test'], i, debug, logs)
    return [row['task_id'], check_result]

async def run(model, limit, debug, logs):
    dataset = load_dataset("openai_humaneval")["test"]
    result = []

    run_fn = None
    if model == "gitlab":
        run_fn = call_gitlab_api
    elif model == "openai":
        run_fn = call_openai_api
    elif model == "codellama":
        run_fn = call_codellama_api
    elif model == "stable-code":
        run_fn = call_stable_code_api
    elif model == "dolphin-mistral":
        run_fn = call_dolphin_mistral_api
    elif model == "deepseek-coder":
        run_fn = call_deepseek_coder_api
    elif model == "claude":
        run_fn = call_claude_api
    elif model == "claude-messaging":
        run_fn = call_messaging_api
    else:
        print("Model not supported")
        sys.exit(1)

    futures = []
    for i in range(limit):
        row = dataset[i]
        futures.append(run_test(row, run_fn, i, debug, logs))
    result = await tqdm_asyncio.gather(*futures)
    
    passed = 0
    for record in result:
        if record[1] == "Passed":
            passed += 1

    print(tabulate(result, ["Task", "Status"], tablefmt="grid"))
    print(f"Total Passed: {passed}/{len(result)}")    

def main(model="gitlab", limit=10, debug=False, logs="./logs"):
    asyncio.run(run(model, limit, debug, logs))

if __name__ == '__main__':
  fire.Fire(main)